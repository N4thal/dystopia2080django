from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views import defaults as default_views
from django.views.generic import TemplateView

urlpatterns = [
    path("", TemplateView.as_view(template_name="pages/home.html"), name="home"),
    path(
        "about/", TemplateView.as_view(template_name="pages/about.html"), name="about"
    ),
    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),
    # User management
    path("accounts/", include("allauth.urls")),
    path("summernote/", include("django_summernote.urls")),
    path("users/", include("dystopia2080.users.urls", namespace="users")),
    path("character/", include("dystopia2080.character.urls")),
    path("character-sheet/", include("dystopia2080.character_sheet.urls")),
    path("item/", include("dystopia2080.item.urls")),
    path("npc/", include("dystopia2080.npc.urls")),
    path("price/", include("dystopia2080.price.urls")),
    path("rules/", include("dystopia2080.rules.urls")),
    path("spell/", include("dystopia2080.spell.urls")),
    path("story/", include("dystopia2080.story.urls")),
    path("timeline/", include("dystopia2080.timeline.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
