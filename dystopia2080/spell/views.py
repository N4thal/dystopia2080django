from django.shortcuts import get_object_or_404, render

from .models import Spell


def spell_index(request):
    spells = Spell.objects.filter(status=True)
    context = {
        "spells": spells,
    }
    return render(request, "spell/index.html", context)


def spell_detail(request, slug):
    spell = get_object_or_404(Spell, slug=slug)

    return render(
        request,
        "spell/deteiled.html",
        {
            "spell": spell,
        },
    )
