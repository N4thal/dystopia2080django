from django.urls import path

from . import views

urlpatterns = [
    path("", views.spell_index, name="spells"),
    path("<slug:slug>", views.spell_detail, name="spell-detail"),
]
