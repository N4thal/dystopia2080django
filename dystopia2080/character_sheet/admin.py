from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import Character, CharacterSkill, CharacterStat, Skill, Stat


class CharacterAdmin(SummernoteModelAdmin):
    list_display = ("name", "user")
    list_filter = ("name", "user")
    search_fields = ("name", "user")


class CharacterSkillAdmin(SummernoteModelAdmin):
    list_display = ("character", "skill", "value")
    list_filter = ("character", "skill")
    search_fields = ("character", "skill")


class CharacterStatAdmin(SummernoteModelAdmin):
    list_display = ("character", "stat", "value")
    list_filter = ("character", "stat")
    search_fields = ("character", "stat")


class DescriptionAdmin(SummernoteModelAdmin):
    summernote_fields = ("description",)


class StatAdmin(DescriptionAdmin):
    list_display = ("name",)
    list_filter = ("name",)
    search_fields = ("name",)


admin.site.register(Character, CharacterAdmin)
admin.site.register(CharacterSkill, CharacterSkillAdmin)
admin.site.register(CharacterStat, CharacterStatAdmin)
admin.site.register(Skill, StatAdmin)
admin.site.register(Stat, StatAdmin)
