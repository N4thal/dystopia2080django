from django.urls import path

from . import views

urlpatterns = [
    path("avatar", views.avatar, name="avatar"),
    path("charactersheet", views.character_sheet, name="character_sheet"),
    path("inventory", views.inventory, name="inventory"),
    path("skills", views.skills, name="skills"),
]
