import math

from django.shortcuts import render

from dystopia2080.character_sheet.models import Character, CharacterSkill
from dystopia2080.Message import Message


def avatar(request):
    context = {}
    return render(request, "character_sheet/avatar.html", context)


def character_sheet(request):
    context = {}
    return render(request, "character_sheet/character_sheet.html", context)


def inventory(request):
    context = {}
    return render(request, "character_sheet/inventory.html", context)


class ViewSkill:
    def __init__(self, name: str, value: int, stat_value: int) -> None:
        self.value = value
        self.name = name
        self.total = value + stat_value


def skills(request):
    try:
        character = request.user.character.get()
    except Character.DoesNotExist:
        return render(
            request,
            "pages/home.html",
            {
                "messages": [
                    Message(
                        "Karakter wurde noch nicht erstellt. Bitte Admin oder Gamemaster informieren.",
                        "danger",
                    )
                ]
            },
        )
    stats = [(stat, []) for stat in character.stats.all()]
    for stat, l in stats:
        for skill in stat.stat.skill_set.all():
            try:
                character_skill = skill.characterskill_set.get(character=character)
            except CharacterSkill.DoesNotExist:
                l.append(ViewSkill(skill.name, -1, stat.value))
                continue
            l.append(ViewSkill(skill.name, character_skill.value, stat.value))
    context = {
        "character": character,
        "stat_width": math.floor(10000 / len(stats)) / 100,
        "stats": stats,
    }
    return render(request, "character_sheet/skills.html", context)
