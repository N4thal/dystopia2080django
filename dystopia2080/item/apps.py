from django.apps import AppConfig


class ItemConfig(AppConfig):
    name = "dystopia2080.item"
