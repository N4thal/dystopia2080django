from django.shortcuts import get_object_or_404, render

from .models import Armor, Weapon


def item_index(request):
    weapons = Weapon.objects.filter(status=True)
    armors = Armor.objects.filter(status=True)
    context = {
        "weapons": weapons,
        "armors": armors,
    }
    return render(request, "item/index.html", context)


def weapon_detail(request, slug):
    weapon = get_object_or_404(Weapon, slug=slug)

    return render(
        request,
        "item/weapon.html",
        {
            "weapon": weapon,
        },
    )


def armor_detail(request, slug):
    armor = get_object_or_404(Armor, slug=slug)

    return render(
        request,
        "item/armor.html",
        {
            "armor": armor,
        },
    )
