from typing import Iterable, Tuple

from django.db import models
from django.urls import reverse

STATUS = (
    (0, "Draft"),
    (1, "Public"),
)


class DamageType(models.Model):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name


class Ammunition(models.Model):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name


class Rarity(models.Model):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name


class ArmorSlot(models.Model):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name


class ArmorType(models.Model):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name


class Armor(models.Model):
    name = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(max_length=255, unique=True)
    icon = models.ImageField(blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True)
    publication_date = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=STATUS, default=0)

    rarity = models.ForeignKey(Rarity, on_delete=models.DO_NOTHING)
    slot = models.ForeignKey(ArmorSlot, on_delete=models.DO_NOTHING)
    armor_type = models.ForeignKey(ArmorType, null=True, on_delete=models.DO_NOTHING)

    projectile_protection = models.IntegerField(default=0)
    melee_protection = models.IntegerField(default=0)
    special_protection = models.IntegerField(default=0)

    additional_effects = models.TextField()

    equipment_points_required = models.IntegerField()
    equipment_points_unlocks = models.CharField(max_length=30)

    strain_effects = models.TextField()

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("armor", kwargs={"slug": str(self.slug)})

    @property
    def fitting(self) -> Iterable[Tuple[str, int, str]]:
        last_one = len(self.equipment_points_unlocks)
        for header, body in enumerate(self.equipment_points_unlocks):
            header += 1
            if header < self.equipment_points_required:
                alert = "danger"
            elif header == last_one:
                alert = "success"
            else:
                alert = "warning"
            yield alert, header, body


class Weapon(models.Model):
    name = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(max_length=255, unique=True)
    icon = models.ImageField(blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True)
    publication_date = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=STATUS, default=0)

    rarity = models.ForeignKey(Rarity, on_delete=models.DO_NOTHING)
    ammunition = models.ForeignKey(Ammunition, on_delete=models.DO_NOTHING)
    ammunition_count = models.IntegerField(default=0)
    precision = models.CharField(max_length=255)
    damage = models.CharField(max_length=255)
    damage_type = models.ForeignKey(DamageType, on_delete=models.DO_NOTHING)
    rate_of_fire = models.IntegerField(default=1)
    reload_ap = models.CharField(default="", max_length=255)
    ap = models.IntegerField()
    max_range = models.CharField(max_length=255)
    range_table = models.TextField(blank=True)
    hit_table = models.TextField(default="")
    pierce = models.IntegerField(default=0)
    equip_ap = models.IntegerField()

    critical_condition = models.CharField(max_length=255)
    critical_effect = models.CharField(max_length=255)
    miscellaneous = models.TextField(blank=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("weapon", kwargs={"slug": str(self.slug)})
