from django.db import models


class FloatingText(models.Model):
    name = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(max_length=255, unique=True)
    last_modified = models.DateTimeField(auto_now=True)
    content = models.TextField()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("floating-detail", kwargs={"slug": str(self.slug)})
