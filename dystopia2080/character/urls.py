from django.urls import path

from dystopia2080.character import views

urlpatterns = [
    path("", views.character_index, name="characters"),
    path("s/<slug:slug>/", views.character_detail, name="character-detail"),
]
