from django.shortcuts import get_object_or_404, render

from .models import Character


def character_index(request):
    characters = Character.objects.filter(status=True)
    context = {
        "characters": characters,
    }
    return render(request, "character/index.html", context)


def character_detail(request, slug):
    template_name = "character/detailed.html"
    character = get_object_or_404(Character, slug=slug)

    return render(
        request,
        template_name,
        {
            "character": character,
        },
    )
