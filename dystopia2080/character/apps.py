from django.apps import AppConfig


class CharacterConfig(AppConfig):
    name = "dystopia2080.character"
