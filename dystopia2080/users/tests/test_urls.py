import pytest
from django.urls import resolve, reverse

from dystopia2080.users.models import User

pytestmark = pytest.mark.django_db


def test_detail(user: User):
    assert reverse(
        "users:detail", kwargs={"username": user.username}
    ) == "/users/{}/".format(user.username)
    assert resolve("/users/{}/".format(user.username)).view_name == "users:detail"


def test_update():
    assert reverse("users:update") == "/users/~update/"
    assert resolve("/users/~update/").view_name == "users:update"


def test_redirect():
    assert reverse("users:redirect") == "/users/~redirect/"
    assert resolve("/users/~redirect/").view_name == "users:redirect"
