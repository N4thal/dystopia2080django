from django.urls import path

from . import views

urlpatterns = [
    path("", views.npc_index, name="npc"),
    path("s/<slug:slug>/", views.npc_detail, name="npc-detail"),
]
