from django.apps import AppConfig


class NpcConfig(AppConfig):
    name = "dystopia2080.npc"
