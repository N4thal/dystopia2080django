from django.apps import AppConfig


class RulesConfig(AppConfig):
    name = "dystopia2080.rules"
