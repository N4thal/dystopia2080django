from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="rules"),
    path("a/<slug:slug>", views.action, name="action-detail"),
    path("c/<slug:slug>", views.class_, name="class-detail"),
    path("l", views.level_up, name="level-up"),
    path("p", views.perks, name="perks"),
]
