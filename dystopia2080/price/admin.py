from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import Prices


class PostAdmin(SummernoteModelAdmin):
    list_display = ("name", "base", "min")
    list_filter = ("status", "publication_date")
    search_fields = ("name",)


admin.site.register(Prices, PostAdmin)
