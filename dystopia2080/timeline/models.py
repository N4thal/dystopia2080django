from django.db import models


class Year(models.Model):
    story_date = models.DateField()
    last_modified = models.DateTimeField(auto_now=True)
    content = models.TextField()

    class Meta:
        ordering = ["story_date"]

    def __str__(self):
        return str(self.story_date.year)
