from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import Year


class PostAdmin(SummernoteModelAdmin):
    summernote_fields = ("content",)
    list_display = ("story_date",)
    list_filter = ("story_date",)
    search_fields = ("story_date", "content")


admin.site.register(Year, PostAdmin)
