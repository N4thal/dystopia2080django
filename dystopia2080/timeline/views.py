from django.shortcuts import render

from .models import Year


def post_index(request):
    year = Year.objects.all()
    context = {
        "years": year,
    }
    return render(request, "timeline/index.html", context)
