from django.urls import path

from dystopia2080.story import views

urlpatterns = [
    path("", views.post_index, name="story"),
    path("s/<slug:slug>/", views.post_detail, name="story-detail"),
]
