from django.apps import AppConfig


class StoryConfig(AppConfig):
    name = "dystopia2080.story"
